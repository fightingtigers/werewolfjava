package peer2peer;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.channels.SocketChannel;

public class Player {

	//Player params BEFORE game starts (for basic connection)
	private String alias;
	private String ipAddress;
	private boolean isHost = false;  //default of false
	
	//to be modified at game start or during the game
	public boolean hasVoted = false;
	private boolean isWerewolf = false;
	private boolean isAlive = true;
	private int numVotes = 0;
	public String line = "";
	
	//Client socket variables
	Socket mySocket;
	DataOutputStream myOutput;
	BufferedReader myInput;
	
	//Server Socket variables
	private DataOutputStream outBuffer;
	private BufferedReader inBuffer;
	private Socket clientSocket;
	
	private boolean heartBeat = true;
	
	//Constructor for player
	public Player(String alias, String ipAddress, boolean isHost){
		Driver.debug("Creating new Player(ip, name, host): " + ipAddress);
		this.alias = alias;
		this.ipAddress = ipAddress;
		this.isHost = isHost;
		createConnection(ipAddress);		
	}

	
	public Player(Player p){
		Driver.debug("Creating new Player(copy): " + ipAddress);
		alias = p.getAlias();
		ipAddress = p.getIp();
		isHost = p.isHost();
		createConnection(ipAddress);
	}
	
	public void closeConnections(){
		try {
			mySocket.close();
			myOutput.close();
			myInput.close();
		} catch (IOException e) {
			e.printStackTrace();;
		}
		  catch (NullPointerException n){
			  System.out.println("Connection already closed!");
		  }

	}
	
	private void createConnection(String ipAddr){
		Driver.debug("Creating connections to new player");
		try {
			if(!(ipAddr.equals(InetAddress.getLocalHost().getHostAddress()))){
				mySocket = new Socket(ipAddr, Network.myPort);
				myOutput = new DataOutputStream(mySocket.getOutputStream());
				myInput = new BufferedReader (new InputStreamReader(mySocket.getInputStream()));

			}
		} catch (UnknownHostException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		} 
		
	}
	public void gameInit(){
		isWerewolf = false;
		isAlive = true;
		numVotes = 0;
		
	}
	
	public void clearVote(){
		numVotes = 0;
	}
	
	
	public void vote()
	{
		numVotes++;
	}
	
	public int getVotes(){
		return numVotes;
	}
	
	public void setWerewolf(boolean isWerewolf)
	{
		this.isWerewolf = isWerewolf;
	}
	
	public boolean isWerewolf()
	{
		return isWerewolf;
	}
	
	public void kill()
	{
		isAlive = false;
	}
	
	public boolean isAlive()
	{
		return isAlive;
	}
	
	public boolean isHost()
	{
		return isHost;
	}
	
	public void setHost(boolean isHost)
	{
		this.isHost = isHost;
	}
	
	public void setAlias(String alias)
	{
		this.alias = alias;
	}
	
	public String getAlias()
	{
		return alias;
	}
	
	public void setIp(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	
	public String getIp(){
		return ipAddress;
	}
	
	//true = alive, false = dead
	public boolean getHeartBeat(){
		return heartBeat;
	}
	public void setHeartBeat(Boolean b){
		heartBeat = b;
	}
	
}
