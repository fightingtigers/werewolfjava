package peer2peer;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class PlayerList {
	public HashMap<String, Player> plist;
	public Player me;
	public Player host;
	public PlayerList(Player m){
		Driver.debug("Creating a new player list");
		plist = new HashMap<String, Player>();
		me = m;
	}
	public void setHost(Player h){
		host = h;
	}
	
	public void addPlayer(Player p){
		if(!plist.containsKey(p.getIp()) && !me.getIp().equals(p.getIp())){
			Driver.debug("Player not yet added: " + p.getIp());
			plist.put(p.getIp(), new Player(p));
		}
	}
	
	public void addPlayer(String name, String ip, boolean isHost){
		if(!plist.containsKey(ip) && !me.getIp().equals(ip)){
			Driver.debug("Player not yet added: " + ip);
			plist.put(ip, new Player(name, ip, isHost));
		}
	}


	public void removePlayer(String ip){
		if(plist.containsKey(ip)){
			GUI.userListBox.remove(plist.get(ip).getAlias());
			boolean lhost = plist.get(ip).isHost();
			plist.get(ip).closeConnections();
			plist.remove(ip);
			
			if(lhost){
				electNewHost();
			}
		}
		
	}
	
	//Add method for deleting player list
	public void clearPlayerList(){
		for(String p: getPlayerIPs()){
			removePlayer(p);
		}
	}
	
	public void clearPlayerVotes(){
		me.clearVote();
		for (String p : plist.keySet()){
			plist.get(p).clearVote();
		}
	}


	public String[] getPlayerIPs(){
		String[] playerIps = new String[plist.size()];
		Set<String> playerSet = plist.keySet();
		Iterator<String> itr = playerSet.iterator();
		for(int i = 0; i < plist.size(); i++){ 
			playerIps[i] = itr.next();
		}
		return playerIps;
	}
	
	
	public void electNewHost(){
		String lowestPlayer = me.getIp();
		int lowestIp = Integer.parseInt(lowestPlayer.replace(".", ""));
		String currentPlayer;
		int currentIp;
		for(String i: getPlayerIPs()){	
			currentPlayer = i;
			currentIp = Integer.parseInt(currentPlayer.replace(".", ""));
			if(lowestIp > currentIp)	{				
				lowestIp = currentIp;
				lowestPlayer = currentPlayer;
			}
		}
		if(lowestPlayer.equals(me.getIp())){
			host = me;
			me.setHost(true);		
			Driver.debug(me.getAlias()+" is the new host");
		}else{
			host = plist.get(lowestPlayer);
			plist.get(lowestPlayer).setHost(true);		
			Driver.debug(plist.get(lowestPlayer).getAlias()+" is the new host");
		}
	}
}
