package peer2peer;

import java.io.BufferedReader;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.*;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Network implements Runnable{

	//Networking Variables
	public static final int myPort = 5555;
	private String ipAddr = "127.0.0.1";
	public static int BUFFERSIZE = 60000;
	ByteBuffer inBuffer = null;
	ByteBuffer outBuffer = null;
	CharBuffer cBuffer = null;
	Charset charset = null;
	CharsetDecoder decoder = null;
	CharsetEncoder encoder = null;

	//User input variables
	private Scanner reader;

	//public static Listener pThread;	
	public PlayerList players;

	//Game Object
	Driver driver;
	public boolean gameStarted = false;
	public String[] playerSnapshot;

	//Listener variables
	private ServerSocketChannel channel = null;
	private Selector selector = null;
	private SocketChannel cchannel = null;
	InetSocketAddress isa = null;
	Set<SelectionKey> readyKeys;
	Iterator<SelectionKey> readyItor;

	private boolean terminated = false;

	/* Initializes network variables. 
	 * 
	 */
	public Network(Driver d){
		try{			
			selector = Selector.open();
			channel = ServerSocketChannel.open();
			channel.configureBlocking(false);
			isa = new InetSocketAddress(myPort);
			channel.socket().bind(isa);
			channel.register(selector, SelectionKey.OP_ACCEPT);
			charset = Charset.forName("us-ascii");
			decoder = charset.newDecoder();
			encoder = charset.newEncoder();
			driver = d;
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	//Creates you as a player and creates an new player list
	private void createMe(String alias, boolean isHost) throws IOException{
		Driver.debug("Creating you as a player");
		Driver.debug("Initializing an empty player list");
		String myIP=InetAddress.getLocalHost().getHostAddress();
		players = new PlayerList(new Player(alias, myIP, isHost));	
	}

	/* Crates you as a player
	 * Creates the host as a player, which creates a connection to the host
	 * Sends the host my player information
	 * Starts the heartbeat thread
	 * called from GUI
	 */
	public void joinGame(String alias, String hostIPAddr){
		Driver.debug("Joining a game...");
		try {
			createMe(alias, false);
			players.host =new Player("Host",hostIPAddr, true);
			players.plist.put(hostIPAddr, players.host);
			sendMessage(Parser.formatPlayer(players.me), players.host);
			HeartBeat hb = new HeartBeat(this, driver);
			Thread heart = new Thread(hb);
			heart.start();
			//Finish later - integrate with Driver class
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	
	/*Creates you as a player
	 * Starts the heartbeat thread
	 * called from GUI
	 */
	public void hostGame(String alias){
		Driver.debug("Hosting a game...");
		try {
			createMe(alias, true);
			HeartBeat hb = new HeartBeat(this, driver);
			Thread heart = new Thread(hb);
			heart.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
	}

	//Pass this function a string and it will send it to all other players
	public void sendMessageAll(String message){
		try {
			for(String i: players.getPlayerIPs()){
				if(!players.me.getIp().equals(i)){
					Driver.debug("    Sending message: \"" + message + "\"to IP: " + i);
					Thread.sleep(100);
					players.plist.get(i).myOutput.writeBytes(message);
				}
			}
		}catch(IOException e1) {
			System.out.println("Output stream error");

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	//Pass this function a string and a player to send that player the string
	public void sendMessage(String message, Player p){
		try {
			if(!players.me.getIp().equals(p.getIp())){
				p.myOutput.writeBytes(message);
				p.myOutput.flush();
				Driver.debug("Sending message: \"" + message + " to IP: " + p.getIp());
			}
		}catch(IOException e1) {
			e1.printStackTrace();
		}
	}
	
	//Send everyone to the new player
	public void sendPlayerList(Player p){
		try{
			Driver.debug("Sending Player List: ");
			sendMessage("Tplayer"+ "\t" + players.me.getAlias() +"\t"+ players.me.getIp() +"\t"+ Boolean.toString(players.me.isHost()) +"\n", p);
			Thread.sleep(100);
			
			for(String i: players.getPlayerIPs()){
				sendMessage(Parser.formatPlayer(players.plist.get(i)), p);
				Thread.sleep(100);
			}
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
	}
	
	//send the list of werewolves to requesting user for consistency purposes
	public void sendWerewolfInfo(Player p){
		Driver.debug("Sending werewolf list");
		String werewolves = "TcheckWerewolf\t";
		
		if(players.me.isWerewolf()){
			werewolves = werewolves + players.me.getIp()+"\t";
		}
		
		for(String i: players.getPlayerIPs()){
			if(players.plist.get(i).isWerewolf()){
				werewolves = werewolves + i + "\t";
			}
		}		
		werewolves = werewolves + "\n";
		sendMessage(werewolves,p);
	}
	
	//Send the new player to everyone
	public void updatePlayerList(Player p){
		Driver.debug("Sending new player to everyone else");
		sendMessageAll(Parser.formatPlayer(p));
	}

	//Sends a chat message to everyone
	public void chat(String message){
		Driver.debug("Network, chat");
		sendMessageAll(Parser.formatChat(message, players.me));
	}

	public void run(){
		try{
			Driver.debug("Network, runListener");
			while(!terminated) {
				selectorIterator();
			}
			Set<SelectionKey> keys = selector.keys();
			Iterator<SelectionKey> itr = keys.iterator();
			System.out.println("Logging out...closing all open connections");
			SelectionKey key;
			while(itr.hasNext()){
				key = (SelectionKey)itr.next();
				if(key.isValid()){
					((ServerSocketChannel)key.channel()).socket().close();

				}
			}
		}catch(IOException e){
			e.printStackTrace();
		}catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void selectorIterator() throws IOException, InterruptedException{
		if(selector.select(500)<0){
			System.out.println("select() failed");
			System.exit(1);
		}
		//Get set of ready sockets
		readyKeys = selector.selectedKeys();
		readyItor = readyKeys.iterator();

		while(readyItor.hasNext()){
			// Get key from set
			SelectionKey key = (SelectionKey)readyItor.next();

			// Remove current entry
			readyItor.remove();

			//Accept new connections, if any
			if(key.isAcceptable()){
				cchannel = ((ServerSocketChannel)key.channel()).accept();
				cchannel.configureBlocking(false);
				String theirIP = Parser.parseKeyString(cchannel.socket().toString());
				
				//Only accept new connections if game hasn't started. If a player was previously in the game, we allow them to reconnect.
				if(!gameStarted)
				{
					System.out.println("Accept connection from " + cchannel.socket().toString());
					cchannel.register(selector, SelectionKey.OP_READ);
				}
				else if(gameStarted && isInSnapshot(theirIP)){
					System.out.println("Accept reconnection from " + cchannel.socket().toString());
					cchannel.register(selector, SelectionKey.OP_READ);
				}
				else{
					System.out.println("Game already started. Closing connection from" + cchannel.socket().toString());
					cchannel.close();
				}
				
			}else{
				cchannel = (SocketChannel)key.channel();
				if(key.isReadable()){
					reader(key);					
				}
			}
		}
	}

	//Reads input from selection key, passes information to Parser
	private void reader(SelectionKey key)throws IOException, InterruptedException{
		Driver.debug(Parser.formatKeyString(key.channel().toString()));		
		
		inBuffer = ByteBuffer.allocateDirect(BUFFERSIZE);
		cBuffer = CharBuffer.allocate(BUFFERSIZE);
        inBuffer.clear();
		
		
		int bytesRecv = cchannel.read(inBuffer);
		if(bytesRecv < 0){
			System.out.println("Tried to read from closed socket. Closing connection.");
			key.cancel();		
			players.removePlayer(Parser.formatKeyString(key.channel().toString()));
			
		}else{
			String line = "";
			inBuffer.flip();
			decoder.decode(inBuffer, cBuffer, false);
			cBuffer.flip();
			line = line + cBuffer.toString();
	
			if(line.endsWith("\n")){
				String[] messages = line.split("\n");
				for(String l: messages){
					Parser.read(l+"\n",driver);
				}
				inBuffer.flip();
				System.out.println("line: "+line);
				line = "";
			}			
		}
	}


	private int sumIps(){
		int ipSum = Integer.parseInt(players.me.getIp().replace(".", ""));
		for(String i: players.getPlayerIPs()){	
			ipSum += Integer.parseInt(i.replace(".", ""));
		}	
		return ipSum;
	}

	
	/* Takes a snapshot of all currently connected users at game start 
	 * (stores ip address, so only THOSE addresses may reconnect during game)
	 */
	public void snapshot(){
		String[] playerIPs = players.getPlayerIPs();
		String[] temp = new String[playerIPs.length+1];
		
		int counter = 0;		
		for(String i: playerIPs){
			temp[counter] = i;
			counter++;
		}
		temp[temp.length-1] = players.me.getIp();
		playerSnapshot = temp;
		Driver.debug("Player Snapshot size: " + playerSnapshot.length);
	}
	
	//checks if an ip address is in the playerSnapshot array
	public boolean isInSnapshot(String ip){
		Driver.debug("THE IP PASSED IN: "+ip);
		boolean exists = false;
		for(int i = 0; i < playerSnapshot.length; i++){
			Driver.debug("In for loop of isInSnapshot");
			Driver.debug(playerSnapshot[i]);
			if(ip.equals(playerSnapshot[i])){
				Driver.debug("Entered if statement in isInSnapshot");
				exists = true;
				break;
			}
		}		
		return exists;
	}
	
	//eliminate player from snapshot array
	public void removeFromSnapshot(String ip){
		for(int i = 0; i < playerSnapshot.length; i++){
			if(ip.equals(playerSnapshot[i])){
				playerSnapshot[i] = "";
				break;
			}
		}
	}
}
