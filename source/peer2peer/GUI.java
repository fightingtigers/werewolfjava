//Ping can you comment this code?


package peer2peer;

/* This PeerToPeerChat class contains all the interface components for our 
 * chat system. This is the main class that will be called upon execution. 
 */

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CharsetEncoder;
import java.util.Iterator;
import java.util.Set;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import java.awt.CardLayout;
import java.awt.Font;

import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JScrollBar;
import javax.swing.ScrollPaneConstants;
import javax.swing.text.DefaultCaret;
import javax.swing.JList;

import java.awt.List;

public class GUI {

	//Initializing all of the UI components here
	private JFrame frmSimplePpChat;
	public static JTextField textField;	
	public static JTextArea textArea;
	public static JButton btnSend;
	private static JPanel panelMain;
	private static JPanel panelChat;
	private static JTextField hostNameTextField;
	private static JTextField joinNameTextField;
	private static JTextField joinIPTextField;
	private static JPanel panelHost;
	private static JPanel panelJoin;
	private static JButton hostCancelButton;
	private static JButton btnCancel;
	private static JButton btnLogout;
	private static JLabel lblName;
	private static JLabel lblHostDisabledFor;
	private static JLabel lblNewLabel;
	private static JLabel lblUserlist;
	private static JLabel myIPLabel;
	public static JLabel phaseLabel;
	public static List userListBox;

	private Network network;
	private Driver driver;
	public static JButton btnVote;
	public static JButton btnStart;
	
	/**
	 * Launch the application.
	 */


	/**
	 * Create the application.
	 * @throws Exception 
	 */
	public GUI(Network n, Driver d) throws Exception {		
		//start program and GUI
		network = n;
		driver = d;
		initialize();
		frmSimplePpChat.setVisible(true);
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	
	//initialize all of our UI components
	private void initialize() {
		frmSimplePpChat = new JFrame();
		frmSimplePpChat.setResizable(false);
		frmSimplePpChat.setTitle("CPSC 559 - WEREWOLF");
		frmSimplePpChat.setBounds(100, 100, 718, 446);
		frmSimplePpChat.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSimplePpChat.getContentPane().setLayout(new CardLayout(0, 0));
		
		panelMain = new JPanel();
		frmSimplePpChat.getContentPane().add(panelMain, "name_5472547274670");
		panelMain.setLayout(null);
		
		JLabel title = new JLabel("Werewolf");
		title.setFont(new Font("Tahoma", Font.BOLD, 42));
		title.setBounds(236, 13, 280, 44);
		panelMain.add(title);
		
		JButton hostButton = new JButton("HOST");
		hostButton.setBackground(new Color(0, 191, 255));
		hostButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				panelMain.setVisible(false);
				panelHost.setVisible(true);
				btnStart.setEnabled(true);
			}
		});
		hostButton.setFont(new Font("Tahoma", Font.BOLD, 22));
		hostButton.setBounds(77, 129, 251, 159);
		panelMain.add(hostButton);
		
		JButton joinButton = new JButton("JOIN");
		joinButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelMain.setVisible(false);
				panelJoin.setVisible(true);
			}
		});
		joinButton.setBackground(new Color(240, 230, 140));
		joinButton.setFont(new Font("Tahoma", Font.BOLD, 22));
		joinButton.setBounds(373, 129, 251, 159);
		panelMain.add(joinButton);
		
		lblHostDisabledFor = new JLabel("Host not fully functional");
		lblHostDisabledFor.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblHostDisabledFor.setBounds(118, 218, 280, 44);
		panelMain.add(lblHostDisabledFor);
		
		panelHost = new JPanel();
		frmSimplePpChat.getContentPane().add(panelHost, "name_5801442754148");
		panelHost.setLayout(null);
		
		hostNameTextField = new JTextField();
		hostNameTextField.setBounds(165, 127, 374, 40);
		panelHost.add(hostNameTextField);
		hostNameTextField.setColumns(10);
		
		JLabel hostEnterNameLabel = new JLabel("Enter a Name:");
		hostEnterNameLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		hostEnterNameLabel.setBounds(38, 132, 117, 27);
		panelHost.add(hostEnterNameLabel);
		
		//START HOSTING A SERVER AS A SUPER PEER
		JButton hostStartButton = new JButton("START");
		hostStartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(hostNameTextField.getText() == "" || hostNameTextField.getText() == null){
					hostNameTextField.setText("Enter a valid Name...");
				}
				else{
					network.hostGame(hostNameTextField.getText());
					Thread t = new Thread(network);
					t.start();
					Driver.debug("Listener thread spawned");
					
					lblName.setText(network.players.me.getAlias());
					myIPLabel.setText(network.players.me.getIp());
					panelHost.setVisible(false);
					panelChat.setVisible(true);
								
				}
				
			}
		});
		hostStartButton.setForeground(new Color(0, 0, 0));
		hostStartButton.setBackground(new Color(154, 205, 50));
		hostStartButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		hostStartButton.setBounds(194, 225, 141, 48);
		panelHost.add(hostStartButton);
		
		JLabel lblHostingAChat = new JLabel("Host a game of Werewolf");
		lblHostingAChat.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblHostingAChat.setBounds(165, 24, 439, 27);
		panelHost.add(lblHostingAChat);
		
		hostCancelButton = new JButton("CANCEL");
		hostCancelButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelHost.setVisible(false);
				panelMain.setVisible(true);
			}
		});
		hostCancelButton.setForeground(new Color(0, 0, 0));
		hostCancelButton.setBackground(new Color(220, 20, 60));
		hostCancelButton.setFont(new Font("Tahoma", Font.BOLD, 20));
		hostCancelButton.setBounds(363, 225, 141, 48);
		panelHost.add(hostCancelButton);
		
		panelJoin = new JPanel();
		frmSimplePpChat.getContentPane().add(panelJoin, "name_5828009880007");
		panelJoin.setLayout(null);
		
		joinNameTextField = new JTextField();
		joinNameTextField.setColumns(10);
		joinNameTextField.setBounds(223, 112, 319, 40);
		panelJoin.add(joinNameTextField);
		
		JLabel label = new JLabel("Enter a Name:");
		label.setFont(new Font("Tahoma", Font.BOLD, 16));
		label.setBounds(61, 117, 117, 27);
		panelJoin.add(label);
		
		JLabel lblJoiningAChat = new JLabel("Join a game of Werewolf");
		lblJoiningAChat.setFont(new Font("Tahoma", Font.BOLD, 26));
		lblJoiningAChat.setBounds(172, 23, 439, 27);
		panelJoin.add(lblJoiningAChat);
		
		//Button for joining a host
		JButton btnJoin = new JButton("JOIN");
		btnJoin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Grab your info from text fields
				network.joinGame(joinNameTextField.getText(), joinIPTextField.getText());
				Driver.debug("Join Network Completed");
				Thread t = new Thread(network);
				t.start();
				Driver.debug("Listener thread spawned");
				myIPLabel.setText(network.players.me.getIp());
				
				
				lblName.setText(network.players.me.getAlias());
				panelJoin.setVisible(false);
				panelChat.setVisible(true);
			}
		});
		btnJoin.setBackground(new Color(173, 255, 47));
		btnJoin.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnJoin.setBounds(223, 245, 141, 48);
		panelJoin.add(btnJoin);
		
		JLabel joinIPLabel = new JLabel("IP of Host:");
		joinIPLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		joinIPLabel.setBounds(61, 187, 136, 27);
		panelJoin.add(joinIPLabel);
		
		joinIPTextField = new JTextField();
		joinIPTextField.setColumns(10);
		joinIPTextField.setBounds(223, 181, 319, 40);
		panelJoin.add(joinIPTextField);
		
		btnCancel = new JButton("CANCEL");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelJoin.setVisible(false);
				panelMain.setVisible(true);
			}
		});
		btnCancel.setBackground(new Color(220, 20, 60));
		btnCancel.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnCancel.setBounds(401, 245, 141, 48);
		panelJoin.add(btnCancel);
		
		panelChat = new JPanel();
		frmSimplePpChat.getContentPane().add(panelChat, "name_5485483942861");
		panelChat.setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(10, 328, 428, 37);
		panelChat.add(textField);
		textField.setColumns(10);
		
		btnSend = new JButton("SEND");
		btnSend.setBackground(Color.WHITE);
		btnSend.setBounds(450, 328, 93, 37);
		panelChat.add(btnSend);
		btnSend.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		textArea = new JTextArea();
		textArea.setBounds(10, 10, 440, 237);
		panelChat.add(textArea);
		textArea.setBackground(new Color(250, 240, 230));
		textArea.setEditable(false);
		
		//makes it always scroll automatically to newest messages
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		//Adds scrollbar to textArea for scrolling through messages
		JScrollPane sp = new JScrollPane(textArea);
		sp.setSize(529, 237);
		sp.setLocation(10, 80);
		sp.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		sp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panelChat.add(sp);

		
		
		
		
		lblName = new JLabel("");
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblName.setBounds(102, 378, 159, 20);
		panelChat.add(lblName);
		
		lblNewLabel = new JLabel("Your Alias:");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(10, 377, 111, 21);
		panelChat.add(lblNewLabel);
		
		lblUserlist = new JLabel("User-List:");
		lblUserlist.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblUserlist.setBounds(577, 54, 111, 21);
		panelChat.add(lblUserlist);
		
		JLabel lblIp = new JLabel("IP:");
		lblIp.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblIp.setBounds(269, 377, 26, 21);
		panelChat.add(lblIp);
		
		myIPLabel = new JLabel("");
		myIPLabel.setFont(new Font("Tahoma", Font.PLAIN, 16));
		myIPLabel.setBounds(291, 378, 159, 20);
		panelChat.add(myIPLabel);
		
		userListBox = new List();
		userListBox.setBounds(545, 80, 157, 231);
		panelChat.add(userListBox);
		
		phaseLabel = new JLabel("Game Lobby");
		phaseLabel.setFont(new Font("Tahoma", Font.BOLD, 25));
		phaseLabel.setBounds(10, 23, 450, 37);
		panelChat.add(phaseLabel);
		
		btnStart = new JButton("START");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//take snapshot of current players and set gameStarted to true
				network.snapshot();
				network.gameStarted = true;
				
				//TODO: implement other things on game start below
				System.out.println("STARTING GAME");
				Game.gameStart = true;
			
				
			}
		});
		btnStart.setEnabled(false);
		btnStart.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnStart.setBackground(Color.WHITE);
		btnStart.setBounds(555, 364, 147, 37);
		panelChat.add(btnStart);
		
		btnVote = new JButton("VOTE");
		btnVote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//TODO: Add action to the vote button
			}
		});
		btnVote.setEnabled(false);
		btnVote.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnVote.setBackground(Color.WHITE);
		btnVote.setBounds(555, 317, 147, 37);
		panelChat.add(btnVote);
		
		btnSend.addMouseListener(new MouseAdapter() {
			//Clicking on the "Send" button 
			@Override
			public void mouseClicked(MouseEvent arg0){

		        String line = null; 

		        // Get user input from textField and send it
		        line = textField.getText();
		        textArea.append(network.players.me.getAlias()+": " + line+"\n");
		        network.chat(textField.getText());
		        Driver.debug("calling network.send");
		        
		        textField.setText("");        		            
					
			}
		});
	}
	public void removePlayer(String pName){
		userListBox.remove(pName);
	}
}
