package peer2peer;

import java.util.Random;
import java.util.Set;

import javax.swing.JLabel;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Iterator;

/* This class is responsible for game setup, logic, flow, and teardown */
public class Game extends Thread{

	private int numVotes = 0;
	private int wolfCount = 0;
	private Network network;
	private int phasetimeMS = 25000;
	private int villagerCount;
	public boolean isTrial;
	public String playerOnTrial;
	public String playerMurdered = "";
	public static String currentPhase = "DD";
	public static boolean gameStart = false;
	private PhaseTimer phaseTimer;

	public Game(Network network){
		this.network = network;
	}

	//start the thread
	public void run(){
		Driver.debug("Waiting outside of loop" + gameStart);
		while(gameStart == false){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		Driver.debug("Out of game start while loop");
		if(network.players.me.isHost()){
			setup();
		}
		else{
			setup1();
		}
	}

	/* Called by constructor to construct initial gamestate and create Player objects from connection information and divide them into werewolves (floor of 1/3) or villagers (remaining)
    Parameters: Array of correctly initialized (alias and ip) player objects
    Assumptions: Array contains at least the minimum number of players
    Superpeer only */

	private void setupVoteListener(){

		//Clicking on the vote button
		GUI.btnVote.addMouseListener(new MouseAdapter() {
			private String key;
			@Override
			public void mouseClicked(MouseEvent arg0){
				
				//perform action based on current phase; should only do anything during Daytime vote, Daytime Trial and Nighttime vote
				switch(currentPhase){
				case "DD": 	break;

				case "DV":  key = GUI.userListBox.getSelectedItem();
							Driver.debug("Selected:" + key);
							key = find(key).getIp();
							pVote(key);

							network.sendMessageAll("Tv-person\t"+network.players.me.getIp()+"\t"+network.players.plist.get(key).getIp()+"\n");
							GUI.btnVote.setEnabled(false);
							break;
				case "DT": 	lVote();
							network.sendMessageAll("Tv-lynch\t"+network.players.plist.get(playerOnTrial).getIp()+"\n");
							GUI.btnVote.setEnabled(false);
							break;
				case "ND":	break;
				case "NV":  key = GUI.userListBox.getSelectedItem();
							key = find(key).getIp();
							pVote(key);
							network.sendMessageAll("Tv-person\t"+network.players.me.getIp()+"\t"+network.players.plist.get(key).getIp()+"\n");
							GUI.btnVote.setEnabled(false);
							break;
				}

			}
		});
	}

	//Called by the host to setup his game state and broadcast information to other peers
	public void setup(){
		
		//Once game has started, disable the start button
		GUI.btnStart.setEnabled(false);
		setupVoteListener();
		
		//get number of connected players and determine number of roles
		int numplayers = network.players.plist.size()+1;
		int numwolf = 0;
		if(numplayers < 3){
			numwolf = 1;
		}
		else{
			numwolf = (int)Math.floor((double)numplayers/3);
		}
		Random rand = new Random();
		String temp;

		//randomly assign werewolf.length players to be werewolves, rest villagers
		ArrayList<String> keys = new ArrayList<String>(network.players.plist.keySet());
		keys.add(network.players.me.getIp());
		for(int i = 0; i < numwolf; i++){
			int j = rand.nextInt(keys.size()-1);
			temp = network.players.plist.get(keys.get(j)).getIp();
			
			//if the loop has selected an already chosen player, restart the loop for that iteration
			if (network.players.plist.get(temp).isWerewolf()){
				i--;

				continue;
			}
			network.players.plist.get(temp).setWerewolf(true);
			network.sendMessageAll("Twerewolf\t"+network.players.plist.get(temp).getIp()+"\n");
		}

		//send werewolves to other players
		if(network.players.me.isWerewolf()){

			network.sendMessageAll("Twerewolf\t"+network.players.me.getIp()+"\n");
		}

		//Host will let other peers know to initialize their games
		network.sendMessageAll("Tphase\tInit\n");
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		//Switch the phase over to Daytime Discussion, the very first phase of the game
		network.sendMessageAll("Tphase\tDD\n");
		dayDiscussion();
	}

	//Called by a non-host to setup their game state
	public void setup1(){
		setupVoteListener();
		int numplayers = network.players.plist.size()+1;
		int numwolf = (int)Math.floor((double)numplayers/3);
		dayDiscussion();
	}

	//Sets a player to be a werewolf when you receive that message from the host
	public void addWerewolf(String ip){
		if(ip.equals(network.players.me.getIp())){
			GUI.textArea.append("You are a Werewolf.\n");
			network.players.me.setWerewolf(true);
		}
		else{
			network.players.plist.get(ip).setWerewolf(true);
		}
	}

	/*During daytime discussion phase, if a player was murdered the previous night, that message will be displayed.
	 * The host will send out the death message as well as remove that player locally. */
	public void dayDiscussion(){
		
		//Set up the GUI for daytime and remove the player that was murdered at night
		setupGuiDayDiscussion();
		if(!playerMurdered.equals("")){
			if(network.players.me.getIp().matches(playerMurdered)){
				GUI.textArea.append("You have been murdered!\n");
			}
			else{
				GUI.textArea.append(network.players.plist.get(playerMurdered).getAlias() + " has been murdered!\n");
			}

			if (network.players.me.isHost())
			{
				network.sendMessageAll("Tdeath\t"+playerMurdered+"\n");
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//remove the player that was murdered at night
			removePlayer(playerMurdered);
			playerMurdered = "";
		}		
		//Start timer for this phase
		phaseTimer = new PhaseTimer();
		phaseTimer.start();
		
		//loop until timer is over
		while(currentPhase.equals("DD")){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//switch to the next phase
		migratePhase(currentPhase);
	}

	/*During nighttime discussion, only werewolves may talk to each other. 
	 * Villagers are unable to talk and see messages at night */
	public void nightDiscussion(){

		//Setup GUI for nighttime and start the phase timer
		setupGuiNightDiscussion();
		phaseTimer = new PhaseTimer();
		phaseTimer.start();

		//loop until timer is over
		while(currentPhase.equals("ND")){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//switch to the next phase
		migratePhase(currentPhase);
	}
	
	/*During nighttime vote, all werewolves can unanimously vote on a target to kill
	 * This is still during the nighttime, so no villagers are able to see or participate in this */
	public void nightVote(){

		//Clear any player votes from previous phases, setup GUI for voting and start the phase timer
		network.players.clearPlayerVotes();
		setupGuiNightVote();
		phaseTimer = new PhaseTimer();
		phaseTimer.start();

		//loop until timer is over
		while(currentPhase.equals("NV")){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//switch to the next phase
		migratePhase(currentPhase);
	}

	//Player selects who he wants to lynch (or kill if werewolf) during this time
	public void dayVote(){
		
		//Clear all player votes from previous phases, setup the GUI for voting 
		network.players.clearPlayerVotes();
		setupGuiDayVote();
		isTrial = false;
		phaseTimer = new PhaseTimer();
		phaseTimer.start();
		
		//loop until timer is over
		while(currentPhase.equals("DV")){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//switch to the next phsae
		migratePhase(currentPhase);
	}
	
	/*Daytime trial phase for the player who was voted by majority 
	 * Only the player standing trial may speak, while others have the vote button enabled
	 * to vote either guilty if they pressed vote, or innocent if they do not vote here*/
	public void dayTrial(){

		//Clear player votes as well as initialize voting variable
		isTrial = true;
		network.players.clearPlayerVotes();
		numVotes = 0;

		//Setup the GUI for daytime trial and start the phase timer
		setupGuiDayTrial();
		phaseTimer = new PhaseTimer();
		phaseTimer.start();
		
		//loop until timer is over
		while(currentPhase.equals("DT")){
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		//switch to the next phase
		migratePhase(currentPhase);
	}
	
	/* When a player is killed by a werewolf or lynched, the game over state method is called and will
	 * wrap up the game for the user by telling them the game is over, and then close off their client*/
	public void gameOverState(){

		//Setup the game over state for the user who was killed
		GUI.textArea.setVisible(true);
		currentPhase = "GG";
		GUI.btnSend.setEnabled(false);
		GUI.btnVote.setEnabled(false);
		GUI.phaseLabel.setText("Game Over!");
		
		//give them five seconds before shutting down their client
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//close the client, they lost
		System.exit(0);
	}

	/*This method will tally up all of the votes received during the daytime voting phase
	 * The user with the majority of the votes will be put on trial. If there is no majority vote, then
	 * there will be no trial phase, and will skip straight to the nighttime discussion phase */
	private void tallyDayVotes(){
		
		//get your number of votes and set that as default max
		String playerIP = network.players.me.getIp();
		int current = 0;
		int max = network.players.me.getVotes();
		
		//Compare everyone's vote count to your own, and update max
		for(String ip : network.players.plist.keySet()){
			current = network.players.plist.get(ip).getVotes();
			if(current > max){
				max = current;
				playerIP = ip;
			}
		}

		//If there is a majority vote for the player with the most votes, that person is placed on trial
		if (max > Math.floor((network.players.plist.size()+1)/2)){
			isTrial = true;
			playerOnTrial = playerIP;

			//if you are the host, make sure to send out the phase info for consistency sake
			if(network.players.me.isHost()){
				network.sendMessageAll("Tphase\t"+"DT"+"\t"+playerOnTrial+"\n");
			}
		}
	}

	/*This method will tally up all the votes for the player on trial, and if we have a majority of people
	 * voting that player guilty, then that player will be lynched, otherwise they will stay alive and we will
	 * move on to the night time discussion phase*/
	private void tallyDayTrialVotes(){
		
		//check for majority vote for guilty
		if(numVotes > (network.players.plist.size()/2)-1){
			
			//send out death message if you're host for consistency sake
			if (network.players.me.isHost())
			{
				network.sendMessageAll("Tdeath\t"+playerOnTrial+"\n");
			}
			removePlayer(playerOnTrial);
			
			//Append whomever died to the text Area on the GUI for display purposes
			if(!network.players.me.getAlias().equals(playerOnTrial)){
				GUI.textArea.append(playerOnTrial+" was lynched by the village!\n");
			}
			else{
				GUI.textArea.append("You were lynched by the village!\n");
			}
		}
		//reset trial-related values
		isTrial = false;
		playerOnTrial = "";
		numVotes = 0;
	}

	/*This method will tally up the votes of the werewolves based on who they wanted to kill
	 *If there is a majority vote on a player, that player will be killed off*/
	private void tallyNightVotes(){
		
		//Grab the list of all werewolves, including yourself if you are one
		ArrayList<String> werewolves = new ArrayList<String>();
		if(network.players.me.isWerewolf()){
			werewolves.add(network.players.me.getIp());
		}
		for(String ip : network.players.plist.keySet()){
			if(network.players.plist.get(ip).isWerewolf()){
				werewolves.add(ip);
			}
		}			

		//Find the player with the maximum number of votes
		String playerIP = network.players.me.getIp();
		int max = network.players.me.getVotes();
		for(String ip : network.players.plist.keySet()){
			if(network.players.plist.get(ip).getVotes() > max){
				max = network.players.plist.get(ip).getVotes();
				playerIP = ip;
			}
		}
		//murder the player if there is a majority vote by the werewolves
		if (max > Math.floor((werewolves.size()/2)))
		{	
			playerMurdered = playerIP;		
		}
	}

	//Increments vote counter for yourself or another player
	public void pVote(String ip){
		if(ip.equals(network.players.me.getIp())){
			network.players.me.vote();
		}
		else{
			network.players.plist.get(ip).vote();
		}
	}
	
	//Increments lynch votes for the player on trial
	public void lVote(){
		numVotes++;
	}

	//Returns the player found in hash map, or yourself if the IP address matches yours
	public Player find(String alias){
		if(alias.equals(network.players.me.getAlias()))
		{
			return network.players.me;
		}
		for (String p: network.players.plist.keySet())
		{
			if(network.players.plist.get(p).getAlias().equals(alias)){
				return network.players.plist.get(p);
			}
		}
		return null;
	}

	//Remove a player from the game (during game)
	public void removePlayer(String playerIP){
		
		//If IP matches yours, close off your connections and move to game-over state
		if(playerIP.equals(network.players.me.getIp())){
			for(String ip : network.players.getPlayerIPs()){
				network.players.removePlayer(ip);
			}
			network.players.me.closeConnections();
			gameOverState();
		}

		//Else if it's not your IP, remove that player from hash map and if they were a werewolf, decrement wolf count
		else if(network.players.plist.containsKey(playerIP)){
			if(network.players.plist.get(playerIP).isWerewolf()){
				wolfCount--;
			}
			network.players.removePlayer(playerIP);
		}
	}

	//Checks win conditions, and will returns true if game is over, false otherwise
	private void endgame(){
		wolfCount = 0;
		villagerCount = 0;

		//Include yourself in the calculation for win condition
		if (network.players.me.isWerewolf()){
			wolfCount++;
		}
		else{
			villagerCount++;
		}

		//Iterate through hash map and get the villager and wolf count
		for(String ip : network.players.plist.keySet()){
			if(!network.players.plist.get(ip).isWerewolf()){
				villagerCount++;
			}
			else{
				wolfCount++;
			}
		}

		//If no werewolves are left, villagers win
		if (wolfCount == 0){
			GUI.textArea.removeAll();
			GUI.textArea.append("Villagers Win!");
			gameOverState();			
		}

		//If there are less than or equal villagers to werewolves, then werewolves win
		if(villagerCount <= wolfCount){
			GUI.textArea.removeAll();
			GUI.textArea.append("Werewolves Win!");
			gameOverState();
		}
	}

	//Consistency check for peer list in game lobby
	public boolean isSameState(int ipSum, int playerCount){
		if(ipSum != sumIP() || playerCount != countPlayers()){
			network.players.clearPlayerList();
			return false;
		}
		return true;
	}

	//Consistency check for peer list AND game state during game
	public boolean isSameState(int ipSum, int playerCount, int werewolves){
		if(ipSum != sumIP() || playerCount != countPlayers() || werewolves != wolfCount){
			network.players.clearPlayerList();
			return false;
		}
		return true;
	}

	//Sums up the ip addresses of everybody in your peer list (removing the period from the address to sum up)
	public int sumIP(){
		int sum = Integer.parseInt(network.players.me.getIp().replace(".", ""));
		Set<String> playerSet = network.players.plist.keySet();
		Iterator<String> itr = playerSet.iterator();
		while(itr.hasNext()){
			sum += Integer.parseInt(itr.toString().replace(".", ""));
		}
		return sum;
	}

	//Return number of players
	public int countPlayers(){
		return network.players.plist.size()+1;
	}

	//Set up the GUI for the night vote phase
	private void setupGuiNightVote(){
		
		//Werewolves are able to vote at night vote phase
		if (network.players.me.isWerewolf()){
			GUI.phaseLabel.setText("Night Voting");
			GUI.textArea.setVisible(true);
			GUI.btnSend.setEnabled(true);
			GUI.btnVote.setEnabled(true);
		}
		//Otherwise, villagers are unable to participate at night
		else{
			GUI.phaseLabel.setText("Night");
			GUI.textArea.setVisible(false);
			GUI.btnSend.setEnabled(false);
			GUI.btnVote.setEnabled(false);
		}
	}

	//Set up the GUI for the night time discussion
	private void setupGuiNightDiscussion(){
		
		//Werewolves are able to discuss at nighttime
		if (network.players.me.isWerewolf()){
			GUI.phaseLabel.setText("Night Discussion");
			GUI.textArea.setVisible(true);
			GUI.btnSend.setEnabled(true);
			GUI.btnVote.setEnabled(false);
		}
		//Otherwise, villagers are unable to participate at night
		else{
			GUI.phaseLabel.setText("Night");
			GUI.textArea.setVisible(false);
			GUI.btnSend.setEnabled(false);
			GUI.btnVote.setEnabled(false);
		}
	}

	//Set up the GUI for daytime discussion phase
	private void setupGuiDayDiscussion(){
		
		//Clear the text area, and set GUI visible for villagers whom had it disabled at night
		GUI.textArea.removeAll();
		GUI.textArea.setVisible(true);
		GUI.btnSend.setEnabled(true);
		GUI.btnVote.setEnabled(false);
		GUI.phaseLabel.setText("Day Discussion");
	}

	//Set up the GUI for daytime vote
	private void setupGuiDayVote(){
		GUI.textArea.setVisible(true);
		GUI.btnSend.setEnabled(true);
		GUI.btnVote.setEnabled(true);
		GUI.phaseLabel.setText("Voting to Stand Trial");

	}
	
	//Set up the GUI for daytime trial phase, if there is one
	private void setupGuiDayTrial(){

		//Only the person on trial is able to talk, rest are able to vote guilty or innocent if they abstain
		GUI.textArea.setVisible(true);
		if(playerOnTrial.equals(network.players.me.getIp())){
			GUI.phaseLabel.setText(network.players.me.getAlias()+"'s trial");
		}
		else{
			GUI.phaseLabel.setText(network.players.plist.get(playerOnTrial).getAlias() + "'s trial");
		}

		if(playerOnTrial.matches(network.players.me.getIp()))
		{
			GUI.btnSend.setEnabled(true);
			GUI.btnVote.setEnabled(false);
		}
		else
		{
			GUI.btnSend.setEnabled(false);
			GUI.btnVote.setEnabled(true);
		}
	}
	
	/* Separate thread ran for the timer of each phase, that will be created and ran at the
	 * start of each phase. When the timer is up, the phase will be switched to the next phase
	 * depending on what the current phase is. */
	public class PhaseTimer extends Thread {

		public void run(){
			try {
				
				//Sleep for phasetimeMS
				Thread.sleep(phasetimeMS);				
				
				//Switch phase depending on what the current phase is
				switch(currentPhase){
				case "DD":	currentPhase = "DV";
							break;
				case "DV":  GUI.btnVote.setEnabled(false);
							tallyDayVotes();
							if(isTrial){
							currentPhase = "DT";
							}
							else{
								currentPhase = "ND";
							}
							break;
				case "DT": 	GUI.btnVote.setEnabled(false);
							tallyDayTrialVotes();
							currentPhase = "ND";
							break;
				case "ND":	currentPhase = "NV";
							break;
				case "NV": 	tallyNightVotes();
							currentPhase = "DD";
							break;
				}
			}catch (InterruptedException e) {
					e.printStackTrace();
			}
		}
	}
	
	/* Migrate phase will handle the switching of the phases for all players once the
	 * phasetimer is up. An action is performed depending on what stage the user is currently on*/
	private void migratePhase(String currentPhase){
		
		//Check for a win condition first
		endgame();
		
		//Perform action and switch the phase depending on the current phase
		switch(currentPhase){
		case "DD":	if (network.players.me.isHost()){
						network.sendMessageAll("Tphase\t"+currentPhase+"\n");
					}
					dayDiscussion();
					break;
		case "DV":	if (network.players.me.isHost()){
						network.sendMessageAll("Tphase\t"+currentPhase+"\n");
					}
					dayVote();
					break;
		case "DT": 	if (network.players.me.isHost()){
						network.sendMessageAll("Tphase\t"+currentPhase+"\t"+playerOnTrial+"\n");
					}
					dayTrial();
					break;
		case "ND":	if (network.players.me.isHost()){
						network.sendMessageAll("Tphase\t"+currentPhase+"\n");
					}
					nightDiscussion();
					break;
		case "NV": 	if (network.players.me.isHost()){
						network.sendMessageAll("Tphase\t"+currentPhase+"\n");
					}
					nightVote();
					break;
		}
	}

	//Return the number of wolves left in the game
	public int getWolfCount() {
		return wolfCount;
	}

	//Update your wolf count
	public void setWolfCount(int wolfCount) {
		this.wolfCount = wolfCount;
	}
}
