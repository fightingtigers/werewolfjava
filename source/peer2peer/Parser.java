package peer2peer;

import java.nio.channels.SelectionKey;

public class Parser {

	
	//Add name check in parser + formatter
	private static void ackPlayer(String s, Driver d){
		String[] tokens = s.split("\t");
		String theirIP = tokens[1].replace("\n", "");
		if(d.network.players.plist.containsKey(theirIP)){
			d.network.players.plist.get(theirIP).setHeartBeat(true);
		}
	}

	public static void read(String s, Driver d){
		//Player message received
		if(s.matches("[T]*player\t[a-zA-Z0-9]+\t[0-9\\.]+\t(true|false)\n")){
			Driver.debug("New Player received");
			checkState(s,d);
			return;
		}
		//Chat message received 
		if(s.matches("[T]*chat\t[a-zA-Z0-9:. ]+\t[a-zA-Z0-9:\\. ]+\n")){
			Driver.debug("New Message received");
			addChat(s);
			return;
		}
		//Check message received
		if(s.matches("[T]*check\t[0-9]+\t[0-9]+\t[0-9]+(\t[a-zA-Z0-9:. ]+)*\n")){
			System.out.println("Receiving check message from host");
			checkPeerList(s,d);

		}
		if(s.matches("[T]*checkWerewolf\t(.+\t)*\n")){

			String[] temp = s.split("\t");
			if(temp.length > 2){
				for(int i = 1; i < temp.length-1; i++){
					if(temp[i] == d.network.players.me.getIp()){
						d.network.players.me.setWerewolf(true);
					}
					else if(d.network.players.plist.containsKey(i)){
						d.network.players.plist.get(i).setWerewolf(true);
					}
				}
			}
			return;
		}
		//Check message returned false
		if(s.matches("[T]*update\t[0-9:.]+\n")){
			updateGameState(s, d);
			return;
		}

		//Ping message
		if(s.matches("[T]*ping\t[0-9.]+\n")){
			sendPong(s, d.network);
			return;
		}

		//Pong message
		if(s.matches("[T]*pong\t[0-9.]+\n")){
			ackPlayer(s, d);
			return;
		}

		if(s.matches("[T]*name\t[a-zA-Z:. ]+\n")){
			changeHostName(s, d.network);
			return;
		}
		if(s.matches("[T]*join\t[a-zA-Z0-9:. ]+\n")){
			//game.addPlayer(cleanString(s));
			return;
		}
		if(s.matches("[T]*disconnect\t[a-zA-Z0-9:. ]+\n")){
			//Mike: when you update Game update this to d.network.players.removePlayer();
			d.game.removePlayer(cleanString(s));
			return;
		}
		if(s.matches("[T]*v-person\t[a-zA-Z0-9:\\. ]+\t[a-zA-Z0-9:\\. ]+\n")){
			//increment (local) vote counter for that player
			String[] tokens = s.split("\t");
			String theirIP = tokens[1];
			System.err.println("Your IP is: "+theirIP);
			String theirAlias = d.network.players.plist.get(theirIP).getAlias();
			System.err.println("Your Alias is: "+theirAlias);
			if(theirAlias == null){
				theirAlias = d.network.players.me.getAlias();
			}
			System.err.println("Your Alias still is: "+theirAlias);
			String whoTheyVotedForIP = tokens[2].substring(0,(tokens[2].length()-1));
			System.err.println("Their IP is: "+whoTheyVotedForIP);
			try{
			String whoTheyVotedFor = d.network.players.plist.get(whoTheyVotedForIP).getAlias();
			System.err.println("Their Alias is: "+whoTheyVotedFor);
			if(whoTheyVotedFor == null){
				whoTheyVotedFor = d.network.players.me.getAlias();
			}
			System.err.println("Their Alias still is: "+whoTheyVotedFor);
			String message = theirAlias + " has voted: " + whoTheyVotedFor + "\n";
			GUI.textArea.append(message);
			System.err.println("Sent "+whoTheyVotedForIP+" to pVote");
			d.game.pVote(whoTheyVotedForIP);
			
			} catch(NullPointerException e){
				//do nothing
			}
			return;
		}
		if(s.matches("[T]*v-lynch\t[a-zA-Z0-9:. ]+\n")){
			d.game.lVote();
			return;
		}
		if(s.matches("[T]*death\t[a-zA-Z0-9:. ]+\n")){
			System.err.println("REMOVE FROM PARSER");
			d.game.removePlayer(cleanString(s));
			return;
		}	
		if(s.matches("[T]*phase\t[a-zA-Z0-9:. ]+(\t[a-zA-Z0-9:. ]+)*\n")){
			String[] temp = s.split("\t");
			String phase = "";
			String msg = "";
			
			if(temp.length == 2){
				phase = cleanString(s);
			}
			else if(temp.length == 3){
				phase = cleanPhaseString(s);
				msg = cleanPhaseString2(s);
			}
			
			//TODO: implement the msg part
			switch(phase){
			case "Init": Game.gameStart = true;
						break;
			case "DD": Game.currentPhase = "DD";
					   break;
			case "DV": Game.currentPhase = "DV";
			           break;
			case "DT": Game.currentPhase = "DT";
					   d.game.playerOnTrial = msg;
			           break;
			case "ND": Game.currentPhase = "ND";
					   break;
			case "NV": Game.currentPhase = "NV";
					   break;
			}
			return;
		}
		if(s.matches("[T]*werewolf\t[a-zA-Z0-9:.]+\n")){
			d.game.addWerewolf(cleanString(s));			
			return;
		}
		
		if(s.matches("[T]*gamestart+\n")){
			//
		}

		System.err.print("Error, message is not recognised: ");
		System.out.println(s);
		return;
	}

	//Methods that change the game state
	public static void addPlayer(String s, Network n){
		String[] tokens = s.split("\t");
		String theirName = tokens[1];
		String theirIP = tokens[2];
		String isHost = tokens[3].replace("\n", "");
		boolean host;
		if(isHost.equals("true")){
			host = true;
		}else{
			host = false;
		}

	//Player newPlayer = new Player(theirName, theirIP, host, false);
		Driver.debug("Player Message Received");
		if(!n.players.plist.containsKey(theirIP)  && !n.players.me.getIp().equals(theirIP)){
			Driver.debug("Player not yet added: (Parser)" + theirIP);
			n.players.addPlayer(theirName, theirIP, host);
			GUI.userListBox.add(theirName);
			if(n.players.me.isHost()){
				n.sendPlayerList(n.players.plist.get(theirIP));
				n.updatePlayerList(n.players.plist.get(theirIP));
			}else if(n.players.plist.get(theirIP).isHost()){
				n.players.host = n.players.plist.get(theirIP);
			}
			
		}else if(n.players.host.getIp().equals(theirIP)){
			n.players.host.setAlias(theirName);
			n.players.plist.get(theirIP).setAlias(theirName);
			GUI.userListBox.add(theirName);			
		}
	}

	public static void addChat(String s){
		String[] tokens = s.split("\t");
		String theirName = tokens[1];
		String theirMessage = tokens[2].substring(0,(tokens[2].length()-1));
		String message = theirName + ": " + theirMessage + "\n";
		GUI.textArea.append(message);            
	}

	private static void checkPeerList(String s, Driver d){
		String[] tokens = s.split("\t");
		
		if(tokens.length == 4){
			int peerIpSum= Integer.parseInt(tokens[1]);
			int peerPlayerCount = Integer.parseInt(tokens[2]);
			int cycleCount = Integer.parseInt(tokens[3].replace("\n", ""));
			
			if(!d.game.isSameState(peerIpSum, peerPlayerCount)){
				d.network.sendMessage("Tupdate\t" + d.network.players.me.getIp() + "\n", d.network.players.host);
			}
		}
		else if(tokens.length == 5){
			int peerIpSum= Integer.parseInt(tokens[1]);
			int peerPlayerCount = Integer.parseInt(tokens[2]);
			int werewolves = Integer.parseInt(tokens[4].replace("\n", ""));
			
			if(!d.game.isSameState(peerIpSum, peerPlayerCount, werewolves)){
				d.network.sendMessage("Tupdate\t" + d.network.players.me.getIp() + "\n", d.network.players.host);
			}
		}
	}

	private static void updateGameState(String s, Driver d){
		String[] tokens = s.split("\t");
		
		//update game state along with peer list if game has begun
		if(d.network.gameStarted){
			d.network.sendPlayerList(d.network.players.plist.get(tokens[1].replace("\n", "")));
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			d.network.sendWerewolfInfo(d.network.players.plist.get(tokens[1].replace("\n", "")));
		}
		//else game hasn't started, so just send your peer list
		else{
			d.network.sendPlayerList(d.network.players.plist.get(tokens[1].replace("\n", "")));
		}		
	}
	private static void sendPong(String s,Network n){
		String[] tokens = s.split("\t");
		Driver.debug(tokens[1].replace("\n", ""));
		try{
		n.sendMessage("Tpong\t" + n.players.me.getIp() + "\n", 
				n.players.plist.get(tokens[1].replace("\n", "")));}
		catch(NullPointerException e){
			//it is fine
		}
	}

	private static void changeHostName(String s, Network n){
		n.players.host.setAlias(cleanString(s));
	}
	
	private static String cleanString(String s){
		String[] tokens = s.split("\t");
		String[] tokens2 = tokens[1].split("\n");
		return tokens2[0];
	}
	
	private static String cleanPhaseString(String s){
		String[] tokens = s.split("\t");
		return tokens[1];
	}
	
	private static String cleanPhaseString2(String s){
		String[] tokens = s.split("\t");
		String[] tokens2 = tokens[2].split("\n");
		return tokens2[0];
	}

	public static String formatIP(String s){		
		String[] token1 = s.split(",");
		String[] token2 = token1[0].split("/");
		return token2[1];
	}

	public static String formatKeyString(String s){
		String[] tokens = s.split("/");
        String[] tokens2 = tokens[1].split(":");
        return tokens2[0];
	}
	//Formating strings to be sent over network
	public static String formatPlayer(Player p){		
		String player = "Tplayer"+"\t" + p.getAlias().trim() +"\t"+ p.getIp().trim() +"\t"+ Boolean.toString(p.isHost()).trim() +"\n";
		return player;
	}
	public static String formatChat(String s, Player p){
		return "Tchat\t"+p.getAlias() +"\t " + s +"\n";
	}

	public static String formatName(String s){
		return "Tname"+"\t"+s+"\n";
	}
	
	//checks current state of system before adding new player

	public static void checkState(String s, Driver d){

		String[] tokens = s.split("\t");

		String theirIP = tokens[2];

		//Only accept new connections if game hasn't started. If a player was previously in the game, we allow them to reconnect.

		if(!d.network.gameStarted){
			addPlayer(s, d.network);
		}
		else if(d.network.gameStarted && d.network.isInSnapshot(theirIP)){
			addPlayer(s, d.network);
		}
		else{
			return;
		}
	}

	//Takes in a selection key object that was converted to string, and parses out the IP address and returns it as string
	public static String parseKeyString(String keyString){
		String[] tokn = keyString.split("/");
        String[] tokn2 = tokn[1].split(",");
        String ip = tokn2[0];      
        return ip;
	}
}



