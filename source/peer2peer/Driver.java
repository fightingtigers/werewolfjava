/* 
 * This class is meant to be the main class
 * it initializes all of the main class objects 
 * it also checks if debug statements should be printed to screen
 */


package peer2peer;

public class Driver {
    //Game Object
    Game game;
    Network network;
    GUI gui;
    public static boolean debug;
    
    public Driver(){
    	try{
    		Driver.debug("Creating the Network...");
	    	network = new Network(this);

	    	Driver.debug("Creating a local game");
	    	game = new Game(network);
	    	
	    	
	    	
	    	Driver.debug("Creating GUI");
	    	gui = new GUI(network,this);
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    }
    public static void main(String[] args){
    	debug = false;
    	if(args.length > 0){
	    	if(args[0].equals("d")){
	    		debug = true;
	    	}
    	}
    	Driver d = new Driver();
    	d.game.start();
    	
    }
    
    public static void debug(String message){
    	if(debug){
    		System.out.println(message);
    	}
    }

}
