package peer2peer;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

public class HeartBeat implements Runnable {

	private Network network;
	private Driver driver;
	public HeartBeat(Network n, Driver d){
		network = n;
		driver = d;
	}
	
	/* This thread is in charge of sending pings to all of the players
	 * It then toggles a player variable. Parser is in charge of receiving the
	 * pong message and toggling back the variable
	 * If the variable has not been toggled back the player is removed on the next round
	 */
	public void run(){
		Driver.debug("[System] Starting HeartBeat Thread");
		try{
			Thread.sleep(1000);
			while(true) {
				for(String player: network.players.getPlayerIPs()){
					if(network.players.plist.containsKey(player)){
						if(network.players.plist.get(player).getHeartBeat() == true){
							network.players.plist.get(player).myOutput.writeBytes("Tping\t"+network.players.me.getIp()+"\n");
							network.players.plist.get(player).setHeartBeat(false);
						}else{
							network.players.removePlayer(player);
							System.out.println("[System] Removing Player due to ungraceful disconnect");
							/*
							//Consistency check upon detecting peer disconnect
							if(network.players.me.isHost()){
								sendCheck();
							}
							else{
								sendUpdateRequest();
							}
							*/
						}
					}
				}
				Thread.sleep(5000);
			}
		}catch (IOException | InterruptedException e) {
			System.out.println("[System] Unable to send heartBeat message (write error)");
		}

	}

	//host sends out a check message to all peers
	private void sendCheck(){
		String message = "Tcheck\t"+driver.game.sumIP()+"\t"+driver.game.countPlayers()+"\t"+"0\n";
		if(network.gameStarted){
			message = "Tcheck\t"+driver.game.sumIP()+"\t"+driver.game.countPlayers()+"\t"+"0"+driver.game.getWolfCount()+"\n";
		}
		network.sendMessageAll(message);
	}

	//peer sends out an update request to the host
	private void sendUpdateRequest(){
		network.players.clearPlayerList();
		network.sendMessage("Tupdate\t"+network.players.me.getIp()+"\n", network.players.host);
	}
}
